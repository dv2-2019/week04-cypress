/// <reference types="Cypress"/>
describe('Test Localhost docker ', () => {
    it('Test1: Mark task as complete', () => {
        cy.visit('http://localhost/')
        cy.get(':nth-child(4) > .card-footer > .row > :nth-child(2) > .nav > :nth-child(3) > form > .btn > .fas').click()
        //test success  
        cy.get('[cl=""] > .nav-link > .fas').should('be.visible')
        cy.get(':nth-child(4) > .card-body > .card-text').should('contain.text', 'KFC')
        cy.get(':nth-child(4) > .card-header > .row > :nth-child(1)').should('contain.text', '\n            Due By:  \n            \n                2020-01-15 \n            \n        ')
        cy.get(':nth-child(4) > .card-header > .row > :nth-child(2) > .badge').should('contain.text', 'Priority: High')
        cy.get(':nth-child(4) > .card-body > .card-title').should('contain.text', 'Eat')
        //complete 
        cy.get(':nth-child(4) > .card-footer > .row > :nth-child(1) > h4 > .badge').should('contain.text', 'Complete')
        cy.get(':nth-child(2) > form > .btn > .fas').should('have.class', 'fas fa-redo-alt')
    })
    it('Test2: Test first task', () => {
        //test success
        cy.get('[cl=""] > .nav-link > .fas').should('be.visible')
        cy.get(':nth-child(2) > .badge').should('contain.text', 'Priority: Normal')
        cy.get(':nth-child(2) > .card-body > .card-title').should('contain.text', 'I want to Sleep')
        cy.get('.card-text').should('contain.text', '24 hrs.')
        cy.get('.card-header > .row > :nth-child(1)').should('contain.text', '\n            Due By:  \n            \n                2020-01-15 \n            \n        ')
        cy.get('h4 > .badge').should('contain.text', 'Incomplete')

    })
})