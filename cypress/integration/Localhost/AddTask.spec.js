/// <reference types="Cypress"/>
describe('Test Localhost docker ', () => {
    it('Test1: Add task', () => {
        cy.visit('http://localhost/')
        cy.get('[href="/task/add"]').click()
        cy.get('#summary').type('Sleep')
        cy.get('#detail').type('24 hrs.')
        cy.get('#duedate').type('2020-01-15')
        cy.get('.btn').click()
        cy.get('[cl=""] > .nav-link > .fas').should('be.visible')
        //test success
        cy.get('.card-text').should('contain.text', '24 hrs.')
        cy.get('.card-header > .row > :nth-child(1)').should('contain.text', '\n            Due By:  \n            \n                2020-01-15 \n            \n        ')
        cy.get(':nth-child(2) > .badge').should('contain.text', 'Priority: Low')
        cy.get(':nth-child(2) > .card-body > .card-title').should('contain.text', 'Sleep')
        cy.get('h4 > .badge').should('contain.text', 'Incomplete')


    })
    it('Test2: Add task2', () => {
        cy.visit('http://localhost/')
        cy.get('[href="/task/add"]').click()
        cy.get('#summary').type('Eat')
        cy.get('#detail').type('KFC')
        cy.get('#priority').select('High')
        cy.get('#duedate').type('2020-01-15')
        cy.get('.btn').click()
        cy.get('[cl=""] > .nav-link > .fas').should('be.visible')
        //test success
        cy.get(':nth-child(4) > .card-body > .card-text').should('contain.text', 'KFC')
        cy.get(':nth-child(4) > .card-header > .row > :nth-child(1)').should('contain.text', '\n            Due By:  \n            \n                2020-01-15 \n            \n        ')
        cy.get(':nth-child(4) > .card-header > .row > :nth-child(2) > .badge').should('contain.text', 'Priority: High')
        cy.get(':nth-child(4) > .card-body > .card-title').should('contain.text', 'Eat')
        cy.get(':nth-child(4) > .card-footer > .row > :nth-child(1) > h4 > .badge').should('contain.text', 'Incomplete')

    })
})