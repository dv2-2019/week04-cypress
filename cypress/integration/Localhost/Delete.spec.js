/// <reference types="Cypress"/>
describe('Test Localhost docker ', () => {
    it('Test1: Delete Task 1', () => {
        cy.visit('http://localhost/')
        //test success : Task1
        cy.get('[cl=""] > .nav-link > .fas').should('be.visible')
        cy.get(':nth-child(2) > .badge').should('contain.text', 'Priority: Normal')
        cy.get(':nth-child(2) > .card-body > .card-title').should('contain.text', 'I want to Sleep')
        cy.get('.card-text').should('contain.text', '24 hrs.')
        cy.get('.card-header > .row > :nth-child(1)').should('contain.text', '\n            Due By:  \n            \n                2020-01-15 \n            \n        ')
        cy.get('h4 > .badge').should('contain.text', 'Incomplete')
        //test success : Task2
        cy.get(':nth-child(4) > .card-body > .card-text').should('contain.text', 'KFC')
        cy.get(':nth-child(4) > .card-header > .row > :nth-child(1)').should('contain.text', '\n            Due By:  \n            \n                2020-01-15 \n            \n        ')
        cy.get(':nth-child(4) > .card-header > .row > :nth-child(2) > .badge').should('contain.text', 'Priority: High')
        cy.get(':nth-child(4) > .card-body > .card-title').should('contain.text', 'Eat')
        cy.get(':nth-child(4) > .card-footer > .row > :nth-child(1) > h4 > .badge').should('contain.text', 'Complete')
        cy.get(':nth-child(2) > form > .btn > .fas').should('have.class', 'fas fa-redo-alt')
        //click delete icon task1
        cy.get(':nth-child(2) > .card-footer > .row > :nth-child(2) > .nav > [cl=""] > .nav-link > .fas').click()
    })
    it('Test2: Click cancel', () => {
        //test delete site
        cy.get(':nth-child(2) > .badge').should('contain.text', 'Priority: Normal')
        cy.get('.card-title').should('contain.text', 'I want to Sleep')
        cy.get('.card-text').should('contain.text', '24 hrs.')
        cy.get('.card-header > .row > :nth-child(1)').should('contain.text', '\n                Due By:  \n                \n                  2020-01-15 \n                  \n              ')
        cy.get('h4 > .badge').should('contain.text', 'Incomplete')

        cy.get('.container > :nth-child(2)').should('contain.text', 'Are you sure you want to delete this task?')
        cy.get('.btn-primary').should('have.class', 'btn btn-primary float-right')
        cy.get('.btn-danger').should('have.class', 'btn btn-danger float-right')
        //click cancel
        cy.get('.btn-primary').click()

    })
    it('Test3: Click icon to delete task again ', () => {
        cy.get(':nth-child(4) > .card-footer > .row > :nth-child(2) > .nav > [cl=""] > .nav-link > .fas').click()
    })
    it('Task4: Click Confirm', () => {
        //test delete site
        cy.get(':nth-child(4) > .card-body > .card-text').should('contain.text', 'KFC')
        cy.get(':nth-child(4) > .card-header > .row > :nth-child(1)').should('contain.text', '\n                Due By:  \n                \n                  2020-01-15 \n                  \n              ')
        cy.get(':nth-child(4) > .card-header > .row > :nth-child(2) > .badge').should('contain.text', 'Priority: High')
        cy.get(':nth-child(4) > .card-body > .card-title').should('contain.text', 'Eat')
        cy.get(':nth-child(4) > .card-footer > .row > :nth-child(1) > h4 > .badge').should('contain.text', 'Complete')

        cy.get('.container > :nth-child(2)').should('contain.text', 'Are you sure you want to delete this task?')
        cy.get('.btn-primary').should('have.class', 'btn btn-primary float-right')
        cy.get('.btn-danger').should('have.class', 'btn btn-danger float-right')

        //click delete
        cy.get('.btn-danger').click()

    })

})